package com.devcamp.splitstring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SplitStringApplication {

	public static void main(String[] args) {
		SpringApplication.run(SplitStringApplication.class, args);
	}

}
