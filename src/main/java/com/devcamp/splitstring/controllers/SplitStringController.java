package com.devcamp.splitstring.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class SplitStringController {
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam(required = true, name = "string") String string) {
        ArrayList<String> splitStringArray = new ArrayList<>();

        String[] stringArray = string.split(" ");

        for (String stringArrayElement : stringArray) {
            splitStringArray.add(stringArrayElement);
        }

        return splitStringArray;
    }
}
